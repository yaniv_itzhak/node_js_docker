FROM node:12

WORKDIR /home/node/app

COPY  package*.json ./

RUN npm install

COPY . . 

EXPOSE 8088

CMD [ "node", "index.js" ]

